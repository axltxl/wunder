/*
 * Wunder API
 * Wunderbank API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Autenticación y manejo de sesiones
 */

 var utils = require ('../util'),
     crypto = require ('crypto'),
     db    = require ('./database');

 // Raiz
 var path_root = '/auth' ;

 //
 var loginDenied = function () {
     return new utils.Error.Unauthorized ("Login denegado");
 };

 //
 var sessionInvalid = function () {
   return new utils.Error.Unauthorized ("Sesión inválida");
 };

 /**
  * Chequeo de sesión
  * Verifica la cookie de sesión de parte del cliente
  */
 exports.check = function (req, res, next) {

   if ( req.url == path_root )
    return next();

   // ¿Existe la cookie?
   if  ( ! ("__wbsid" in req.cookies) )
     return next ( sessionInvalid() );

   // session id
   sessid = req.cookies['__wbsid'];

   // ¿Esa id de session estará registrada en base de datos?
   if ( ! (sessid in db.session) )
    return next ( sessionInvalid() );

   // It's all good buddy
   next();

 };

 /**
  * GET /auth
  *
  * Esta es una vulgar comprobación de la cookie de sesión
  * expresada en una entrada de la API, no es middleware.
  * Devuelve HTTP 200 en caso de que la session id sea válida
  * de lo contrario devolverá 403
  */
 exports.ping = {

   //
   path: path_root,

   //
   callback : function (req, res, next) {
      // ¿Existe la cookie?
      if  ( ! ("__wbsid" in req.cookies) )
        return next ( sessionInvalid() );
      else {
        // session id
        sessid = req.cookies['__wbsid'];

        // ¿Esa id de session estará registrada en base de datos?
        if ( ! (sessid in db.session) )
          return next ( sessionInvalid() );
        else res.send ( 200 );
      }
    }
 };

 /**
  * POST /auth
  *
  * Revisión de credenciales del usuario
  */
 exports.login = {

    // Path
    path: path_root,

    //
    callback: function (req, res, next)
    {
      // Verificación de existencia
      if (! ("username" in req.body && "password" in req.body) )
        return next ( loginDenied() );

      // Asignar credenciales
      var usr_name = req.body['username'];
      var usr_pass = req.body['password'];

      // Verificar nombre de usuario
      if (! (usr_name in db.users) )
        return next ( loginDenied() );

      // Nombre de usuario
      var user = db.users[usr_name];

      // Verificación de contraseña
      if ( usr_pass !== user.passwd  )
        return next ( loginDenied() );

      // Generar sessionID
      // La sessionID es un token por el cual se
      // determina la autenticidad del usuario en
      // cada petición HTTP, esta sessionID estará
      // representada en una cookie del lado del cliente
      // y estará registrada en base de datos de parte
      // del servidor
      var sessionID =  crypto.createHash('sha256')
                        .update(Math.random().toString())
                        .digest("hex");

      // Guardar sessionID en base de datos para este usuario
      db.session[sessionID] = usr_name;

      // Establecer la session cookie hacia el cliente
      res.cookie ( '__wbsid', sessionID );

      // Enviar la respuesta
      res.send ({id : sessionID});
    }
 };

 /**
  * DELETE /session
  *
  * Logout
  */
 exports.logout = {

   // Path
   path : path_root,

  // Callback
   callback : function (req, res, next) {
     next();
   }
 };
