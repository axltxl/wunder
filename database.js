/*
 * Wunder API
 * Wunderbank API
 *
 * Copyright (c) Alejandro Ricoveri
 *
 * Base de datos
 */

 module.exports =
 {
   // Usuarios
   users : {
      "alejandro" : {
          name: "Alejandro Ricoveri",
          email: "alejandroricoveri@gmail.com",
          passwd: 'ale'},
      "carolina"  : {
          name: "Carolina Ricoveri",
          email: "cricoveri@gmail.com",
          passwd: 'carol'},
      "eunice"    : {
          name: "Eunice González",
          email: "eunicecrochet@gmail.com",
          passwd: 'euni'},
      "ornella"   : {
          name: "Ornella Delli-Rocili",
          email: "ornella87@gmail.com",
          passwd: 'orne'},
      "antonio"   : {
          name: "Antonio Ricoveri",
          email: "antonioricoveri@gmail.com",
          passwd: 'tony'}
   },
   
   session : {
   },

   // Cuentas bancarias
   accounts : {
     "010507490886453" : {
        usr_id : "alejandro",
        balance : -230
       },
     "010507490226437" : {
        usr_id : "carolina",
        balance : 112340
       },
     "010507412384518" : {
        usr_id : "alejandro",
        balance : 58120345
       },
   }
 }
